#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    result = AudioDimensionalityReduction::run( "/home/leandro/Data/Audio/anto.mp3" );
}

//--------------------------------------------------------------
void ofApp::update(){

}

//--------------------------------------------------------------
void ofApp::draw(){
    for ( auto audioDataPoint : result ) {
        ofDrawEllipse( ofMap(audioDataPoint[0], 0, 1, 0, ofGetWidth()),
                       ofMap(audioDataPoint[1], 0, 1, 0, ofGetWidth()),
                       5, 5 );
    }
}


//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
//    currentSTFT += 1;

//    if ( currentSTFT >= audios.size() ) {
//        currentSTFT = 0;
//    }

//    doSTFT();
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
