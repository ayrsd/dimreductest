#include "AudioDimensionalityReduction.h"
string AudioDimensionalityReduction::currentLog = "";
string AudioDimensionalityReduction::currentProcess = "";
int AudioDimensionalityReduction::progress = 0;


vector< vector<float> > AudioDimensionalityReduction::doSTFT(vector<float> audioData, int windowSize, int hopSize)
{
 /*
 * Esta implementación tiene diferencias con la librosa.
 * En caso que se quiera hacer exactamente igual:
 * librosa hace "centered": np.pad(audioData,windowSize/4,"reflect")
 * esto hace que el array resultante sea un poco mas largo
 * además agrega 1 más que es el offset
 */

    kiss_fftr_cfg cfg = kiss_fftr_alloc( windowSize, 0 , NULL, NULL );

    vector< vector<float> > stft;
    int i;

    // Create a hamming window of appropriate length
    float window[windowSize];
    hamming(windowSize, window);
    float windowSum = 0;
    for ( i = 0 ; i < windowSize ; i++ ) {
        windowSum += window[i];
    }

    int chunkPosition = 0;
    int readIndex;
    bool bStop = false;
    kiss_fft_cpx fftResult[windowSize/2];

    // Process each chunk of the signal
    while(chunkPosition < audioData.size() && !bStop) {

        float windowedAudio[windowSize];
        // Copy the chunk into our buffer
        for(i = 0; i < windowSize; i++) {
            readIndex = chunkPosition + i;

            if(readIndex < audioData.size()) {
                windowedAudio[i] = audioData[readIndex] * window[i];
            } else {
                // we have read beyond the signal, so zero-pad it!
                windowedAudio[i] = 0.0;
                bStop = true;
            }
        }

        kiss_fftr(cfg, (kiss_fft_scalar*)windowedAudio, fftResult);
        vector<float> fftResultReal;
        float normalizer = 1. / pow(windowSum,2); // asi lo hace scipy


        for ( i = 0 ; i < windowSize / 2 ; i++ ) {
            fftResultReal.push_back( sqrtf(fftResult[i].r * fftResult[i].r + fftResult[i].i * fftResult[i].i) * normalizer );
        }

        stft.push_back( fftResultReal );

        chunkPosition += hopSize;
    }

    kiss_fftr_free(cfg);

    return stft;
}

vector<vector<float> > AudioDimensionalityReduction::doSTFTAll(vector<vector<float> > data, int windowSize, int hopSize)
{
    vector<vector<float>> stftVectors;
    for ( auto audioData : data ) {
        vector< vector<float> > stftData = doSTFT(audioData, windowSize, hopSize);

        //vectorization
        vector<float> stftVectorized;
        for ( auto stftTimeWindow : stftData ) {
            for ( auto freqAmplitude : stftTimeWindow ) {
                stftVectorized.push_back(freqAmplitude);
            }
        }

        stftVectors.push_back( stftVectorized ); //esto debería dar 45100
    }
    return stftVectors;
}

vector<vector<float> > AudioDimensionalityReduction::doPCA(vector<vector<float> > data, int nComponents)
{
    vector<vector <float>> pcaResult;

    auto timeStart = ofGetElapsedTimeMillis();
    ofLog() << "PCA (" << nComponents << " components)...";
    ofxPCA pca;
    ofLog() << data.size();
    pca.calculate(data,nComponents); //aca hay mas parametros que hay que chequear que este todo ok
    ofLog() << data.size();
    ofExit();
    float variabilidad = 0;
    for ( auto v : pca.prop_of_var() ) {
        variabilidad += v;
    }

    ofLog() << "components: " << pca.prop_of_var().size() << " variabilidad: " << variabilidad << " || Threshold 95: " << pca.thresh95() << " keiser: " << pca.kaiser();
    ofLog() << "finished (" << ofGetElapsedTimeMillis() - timeStart << " ms)"; //198,059 segundos | 3.3 minutos

    for ( auto v : data ) {
        ofLog() << "transforming";
        pcaResult.push_back( pca.transform(v) );
    }

    return pcaResult;
}

vector<vector<double> > AudioDimensionalityReduction::doTSNE(vector<vector<float> > data, double perplexity, double theta)
{
    ofxTSNE tsne;
    ofLog() << "tSNE...";
    auto timeStart = ofGetElapsedTimeMillis();
    auto result = tsne.run( data, 2, perplexity, theta, true, false );
    ofLog() << "finished (" << ofGetElapsedTimeMillis() - timeStart << " ms)";
    return result;
}

vector<vector<double>> AudioDimensionalityReduction::run(string dirPath,
                                                         int targetSampleRate,
                                                         float targetDuration,
                                                         int stft_windowSize,
                                                         int stft_hopSize,
                                                         int pca_nComponents,
                                                         double tsne_perplexity,
                                                         double tsne_theta)
{
    vector<string> audioFilesPaths = searchAudioFiles(dirPath);
    ofLog() << audioFilesPaths.size() << " files found";
//    for ( auto p : audioFilesPaths ) {
//        ofLog() << p;
//    }
    vector<vector<float>> rawAudios = loadFilesAndCrop(audioFilesPaths, targetSampleRate, targetDuration);
    vector<vector<float>> stftVectors = doSTFTAll(rawAudios, stft_windowSize, stft_hopSize);
    // NO PCA !! :((
//    vector<vector<float>> pcaResult = doPCA(stftVectors, pca_nComponents);
//    vector<vector<double>> tsneResult = doTSNE(pcaResult, tsne_perplexity, tsne_theta);
    vector<vector<double>> tsneResult = doTSNE(stftVectors, tsne_perplexity, tsne_theta);

    return tsneResult;
}

vector<string> AudioDimensionalityReduction::searchAudioFiles(string dirPath, vector<string> extensions)
{
    ofDirectory dir ( dirPath );
    vector<string> files;
    scanDir(dir, files, extensions);

    return files;
}

void AudioDimensionalityReduction::hamming(int windowLength, float *buffer) {
 for(int i = 0; i < windowLength; i++) {
   buffer[i] = 0.54 - (0.46 * cos( 2 * PI * (i / ((windowLength - 1) * 1.0))));
 }
}

void AudioDimensionalityReduction::scanDir(ofDirectory dir, vector<string> &files, vector<string> extensions)
{
    dir.listDir();

    for(auto file : dir)
    {
        if(file.isDirectory())
        {
            scanDir(ofDirectory(file.getAbsolutePath()), files, extensions);
        }
        else
        {
            string extension = ofToLower( file.getExtension() );
            if ( std::find(extensions.begin(), extensions.end(), extension) != extensions.end() ) {
                files.push_back(file.path());
            }
        }
    }
}

vector<float> AudioDimensionalityReduction::loadFileAndCrop(string filePath, int targetSampleRate, float targetDuration)
{
    vector<float> audioData;

    ofxAudioFile audio;
    audio.load( filePath );

    assert(audio.loaded());
    assert(audio.samplerate() >= targetSampleRate);
    assert(audio.samplerate() % targetSampleRate == 0);

    for ( int i = 0 ; i < audio.length() ; i += audio.samplerate() / targetSampleRate ) {
        audioData.push_back( audio.sample(i,0) );
    }

    string strLog = "";

    float currentDuration = audioData.size() / (float)targetSampleRate;

    if ( currentDuration > targetDuration ) {
        audioData.resize( targetSampleRate * targetDuration );
    } else if ( currentDuration < targetDuration ) {
        while( currentDuration < targetDuration ) {
            audioData.push_back(0);
            currentDuration = audioData.size() / targetSampleRate;
        }
    }

    return audioData;
}

vector<vector<float>> AudioDimensionalityReduction::loadFilesAndCrop(vector<string> filePaths, int targetSampleRate, float targetDuration)
{
    vector< vector<float> > dataReturn;
    for ( auto audioFilePath : filePaths ) {
        dataReturn.push_back( loadFileAndCrop(audioFilePath, targetSampleRate, targetDuration) );
    }
    return dataReturn;
}
