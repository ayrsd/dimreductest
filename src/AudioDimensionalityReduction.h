#pragma once
#include "ofMain.h"
#include "ofxAudioFile.h"
#include "ofxTSNE.h"
#include "ofxPCA.h"
#include "kiss_fftr.h"

class AudioDimensionalityReduction
{
public:
    static string currentProcess;
    static string currentLog;
    static int progress;

    /**
     * @brief run Runs the entire pipeline. Audio files inside dirPath
     * should be at least of audioSampleRate (downsampling will be
     * performed)
     * @param dirPath
     * @param audioSampleRate
     * @param audioDuration
     * @param stft_windowSize
     * @param stft_hopSize
     * @param pca_nComponents
     * @param tsne_perplexity
     * @param tsne_theta
     * @return
     */
    static vector< vector<double> > run( string dirPath,
                                         int targetSampleRate = 22050,
                                         float targetDuration = 1.0, // Seconds. Audios exceeding duration will be chopped, otherwise will be zero padded
                                         int stft_windowSize = 2048,
                                         int stft_hopSize = 2048/4,
                                         int pca_nComponents = 300,
                                         double tsne_perplexity = 30,
                                         double tsne_theta = 0.5);

private:
    static vector<string> searchAudioFiles( string dirPath, vector<string> extensions = {"wav", "mp3"} );
    static vector<float> loadFileAndCrop(string filePath, int targetSampleRate, float targetDuration);
    static vector< vector<float> > loadFilesAndCrop(vector<string> filePaths, int targetSampleRate, float targetDuration);
    static vector< vector<float> > doSTFT(vector<float> audioData, int windowSize = 256, int hopSize = 256);
    static vector< vector<float> > doSTFTAll(vector<vector<float>> data, int windowSize = 256, int hopSize = 256);
    static vector< vector<float> > doPCA( vector< vector<float> > data, int nComponents);
    static vector< vector<double> > doTSNE( vector< vector<float> > data, double perplexity=30, double theta=0.5);

    static void hamming(int windowLength, float *buffer);
    static void scanDir(ofDirectory dir, vector<string> & files, vector<string> extensions);
};
